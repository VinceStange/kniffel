package de.rules;

import de.rules.KniffelEngine;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Created by vstange on 04.10.2016.
 */
public class KniffelEngineTest {

    @Test
    public void calcSame() throws Exception {
        assertThat(KniffelEngine.calcSame(3, new int[] {2, 3, 4, 5, 6}), is(equalTo(3)));
        assertThat(KniffelEngine.calcSame(3, new int[] {5, 4, 3, 2, 1}), is(equalTo(3)));
        assertThat(KniffelEngine.calcSame(4, new int[] {5, 4, 4, 2, 1}), is(equalTo(8)));
    }

    @Test
    public void isPasch() throws Exception {
        assertFalse(KniffelEngine.isPasch(new int[] {6, 3, 4, 3, 2}, 3));
        assertFalse(KniffelEngine.isPasch(new int[] {1, 3, 4, 3, 4}, 3));
        assertTrue(KniffelEngine.isPasch(new int[] {4, 3, 4, 3, 4}, 3));
        assertFalse(KniffelEngine.isPasch(new int[] {4, 3, 4, 3, 4}, 4));
        assertTrue(KniffelEngine.isPasch(new int[] {4, 3, 4, 4, 4}, 4));
    }

    @Test
    public void isFullHouse() throws Exception {
        assertFalse(KniffelEngine.isFullHouse(new int[] {6, 3, 4, 3, 2}));
        assertTrue(KniffelEngine.isFullHouse(new int[] {4, 3, 4, 3, 4}));
    }

    @Test
    public void isKniffel() throws Exception {
        assertTrue(KniffelEngine.isAllSame(new int[] {4, 4, 4, 4, 4}));
        assertFalse(KniffelEngine.isAllSame(new int[] {1, 4, 4, 4, 4}));
        assertFalse(KniffelEngine.isAllSame(new int[] {4, 2, 4, 4, 4}));
        assertFalse(KniffelEngine.isAllSame(new int[] {4, 4, 3, 4, 4}));
        assertFalse(KniffelEngine.isAllSame(new int[] {4, 4, 4, 5, 4}));
    }

    @Test
    public void isLittleStreet() throws Exception {
        assertTrue(KniffelEngine.isLittleStreet(new int[] {6, 4, 2, 3, 5}));
        assertTrue(KniffelEngine.isLittleStreet(new int[] {3, 5, 2, 1, 4}));
        assertTrue(KniffelEngine.isLittleStreet(new int[] {3, 6, 2, 1, 4}));
        assertTrue(KniffelEngine.isLittleStreet(new int[] {3, 3, 4, 2, 5}));
        assertFalse(KniffelEngine.isLittleStreet(new int[] {6, 3, 4, 3, 2}));
        assertFalse(KniffelEngine.isLittleStreet(new int[] {4, 3, 4, 3, 4}));
    }

    @Test
    public void isBigStreet() throws Exception {
        assertTrue(KniffelEngine.isBigStreet(new int[] {6, 4, 2, 3, 5}));
        assertTrue(KniffelEngine.isBigStreet(new int[] {3, 5, 2, 1, 4}));
        assertFalse(KniffelEngine.isBigStreet(new int[] {6, 3, 4, 3, 2}));
        assertFalse(KniffelEngine.isBigStreet(new int[] {4, 3, 4, 3, 4}));
    }

}