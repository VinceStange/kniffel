package de;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Testclass
 */
public class DiceBoxTest {

    @Test
    public void rollDice() throws Exception {
        DiceBox diceBox = new DiceBox();
        for (int i = 0; i < 100; i++)
            assertThat(diceBox.rollDice(6), is(both(greaterThanOrEqualTo(1)).and(lessThanOrEqualTo(6))));

        for (int i = 0; i < 100; i++)
            assertThat(diceBox.rollDice(20), is(both(greaterThanOrEqualTo(1)).and(lessThanOrEqualTo(20))));
    }

    @Test
    public void rollDices() throws Exception {
        DiceBox diceBox = new DiceBox();
        assertThat( diceBox.rollDices(6, 5).length, is(equalTo(5)));
        assertThat( diceBox.rollDices(3, 10).length, is(equalTo(10)));
    }

}