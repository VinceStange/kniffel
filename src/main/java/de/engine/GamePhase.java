package de.engine;

/**
 * Created by vstange on 16.12.2016.
 */
public enum GamePhase {

    Start, Roll, Decide, Result
}
