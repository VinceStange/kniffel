package de.engine;

import de.Dice;
import de.DiceBox;
import de.forms.JKniffelRow;
import de.rules.KniffelEngine;
import de.rules.KniffelRule;

import java.util.ArrayList;
import java.util.List;

/**
 * Very simplistic game engine. It controls the game cycle
 * and notifies each listener of a phase-change.
 * @author vstange on 15.12.2016.
 */
public class GameEngine {

    private DiceBox diceBox = new DiceBox();

    private List<KniffelEventListener> listeners = new ArrayList<>();

    public GameState state = new GameState();

    /**
     * Refresh every UI Listener
     * @param currentPhase {@link GamePhase}
     */
    private void refreshAfterEvent(GamePhase currentPhase) {
        for (KniffelEventListener listener : listeners) {
            listener.refresh(currentPhase, state);
        }
    }

    /**
     * Register a new UI Listener, e.g. the {@link JKniffelRow}
     * @param listener {@link KniffelEventListener}
     */
    public void addListener(KniffelEventListener listener) {
        listeners.add(listener);
    }

    public void roll() {
        if (state.rollCounter >= 3)
            return;

        int[] numbers = diceBox.rollDices(6, state.dices.getFresh().size());
        state.dices.setValues(numbers);
        state.rollCounter++;

        // Nächste Phase einleiten (sind schon 3 Würfe gemacht?
        GamePhase nextPhase = state.rollCounter == 3 ? GamePhase.Decide : GamePhase.Roll;
        refreshAfterEvent(nextPhase);
    }

    public void prepareFreshGame() {
        // Reset the game world
        GamePhase currentPhase = GamePhase.Start;
        state.decideCounter = 0;
        state.rollCounter = 0;
        state.pointsBlockOne = 0;
        state.pointsBonus = 0;
        state.pointsBlockTwo = 0;
        state.pointsOverall = 0;

        // prepare new dices
        state.dices.clear();
        for (int i = 1; i <= 5; i++) {
            state.dices.add(new Dice());
        }

        refreshAfterEvent(currentPhase);
    }

    public void decision(int points, KniffelRule.PointType type) {
        // Punkte vergeben, End-of-Time hochzählen
        state.decideCounter++;

        // Punkte frisch zusammenrechnen
        if (type.equals(KniffelRule.PointType.BlockOne))
            state.pointsBlockOne += points;
        else
            state.pointsBlockTwo += points;
        state.pointsBonus = KniffelEngine.getBonusPoints(state.pointsBlockOne);
        state.pointsOverall = state.pointsBlockOne + state.pointsBlockTwo + state.pointsBonus;

        // Alle Punkte vergeben? Runde beendet?
        boolean isFinish = state.decideCounter == KniffelEngine.rulesCount;

        if (!isFinish) {
            state.rollCounter = 0;
            // prepare new dices
            state.dices.clear();
            for (int i = 1; i <= 5; i++) {
                state.dices.add(new Dice());
            }
            roll();
        } else {
            refreshAfterEvent(GamePhase.Result);
        }

    }

}
