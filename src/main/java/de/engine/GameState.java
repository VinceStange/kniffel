package de.engine;

import de.GameDices;

/**
 * Created by vstange on 15.12.2016.
 */
public class GameState {

    public GameDices dices = new GameDices();

    public int rollCounter = 0;

    public int decideCounter = 0;

    public int pointsBlockOne = 0;

    public int pointsBonus = 0;

    public int pointsBlockTwo = 0;

    public int pointsOverall = 0;

}
