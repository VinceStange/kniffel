package de.engine;

/**
 * Created by vstange on 15.12.2016.
 */
public interface KniffelEventListener {

    void refresh(GamePhase phase, GameState state);

}
