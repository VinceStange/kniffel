package de.rules;

import java.util.function.Function;

/**
 * Created by vstange on 07.12.2016.
 */
public class KniffelRule {

    public enum PointType {
        BlockOne, BlockTwo
    }

    private String display;

    private PointType type;

    private Function<int[], Boolean> valid;

    private Function<int[], Integer> points;

    KniffelRule(String display, PointType type, Function<int[], Boolean> valid, Function<int[], Integer> points) {
        this.display = display;
        this.type = type;
        this.valid = valid;
        this.points = points;
    }

    public String getDisplay() {
        return display;
    }

    public boolean isValid(int[] dices) {
        return valid.apply(dices);
    }

    public int getPoints(int[] dices) {
        return points.apply(dices);
    }

    public PointType getType() {
        return type;
    }
}
