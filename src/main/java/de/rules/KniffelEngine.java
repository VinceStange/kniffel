package de.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static de.rules.KniffelRule.PointType.*;


/**
 * Created by vstange on 04.10.2016.
 */
public class KniffelEngine {

    public static ArrayList<KniffelRule> blockOneRules = new ArrayList<>();

    public static ArrayList<KniffelRule> blockTwoRules = new ArrayList<>();

    public static int rulesCount = 0;

    static {
        blockOneRules.add(new KniffelRule("1er", BlockOne, (d) -> true, (d) -> calcSame(1, d)));
        blockOneRules.add(new KniffelRule("2er", BlockOne, (d) -> true, (d) -> calcSame(2, d)));
        blockOneRules.add(new KniffelRule("3er", BlockOne, (d) -> true, (d) -> calcSame(3, d)));
        blockOneRules.add(new KniffelRule("4er", BlockOne, (d) -> true, (d) -> calcSame(4, d)));
        blockOneRules.add(new KniffelRule("5er", BlockOne, (d) -> true, (d) -> calcSame(5, d)));
        blockOneRules.add(new KniffelRule("6er", BlockOne, (d) -> true, (d) -> calcSame(6, d)));

        blockTwoRules.add(new KniffelRule("Dreierpasch", BlockTwo, (d) -> isPasch(d, 3), (d) -> Arrays.stream(d).sum()));
        blockTwoRules.add(new KniffelRule("Viererpasch", BlockTwo, (d) -> isPasch(d, 4), (d) -> Arrays.stream(d).sum()));
        blockTwoRules.add(new KniffelRule("Full-House", BlockTwo, (d) -> isFullHouse(d), (d) -> 25));
        blockTwoRules.add(new KniffelRule("Kleine Strasse", BlockTwo, (d) -> isLittleStreet(d), (d) -> 30));
        blockTwoRules.add(new KniffelRule("Grosse Strasse", BlockTwo, (d) -> isBigStreet(d), (d) -> 40));
        blockTwoRules.add(new KniffelRule("Kniffel", BlockTwo, (d) -> isAllSame(d), (d) -> 50));
        blockTwoRules.add(new KniffelRule("Chance", BlockTwo, (d) -> true, (d) -> Arrays.stream(d).sum()));

        rulesCount = blockOneRules.size() + blockTwoRules.size();
    }

    public static int getBonusPoints(int blockOnePoints) {
        return blockOnePoints >= 63 ? 35 : 0;
    }

    static int calcSame(int site, int[] dices) {
        int result = 0;
        for (int d : dices) {
            result += d == site ? site : 0;
        }
        return result;
    }

    static boolean isLittleStreet(int[] dices) {
        // Filter doubles, so we can later use 4 unique dices
        Set<Integer> values = new HashSet<>();
        Arrays.stream(dices).forEach(values::add);
        int[] ints = values.stream().mapToInt(i -> i).toArray();
        Arrays.sort(ints);
        // Now just try every combination of 4 dices
        int[] var1 = Arrays.copyOfRange(ints, 0, 4);
        int[] var2 = Arrays.copyOfRange(ints, 1, 5);

        return Arrays.equals(var1, new int[]{1, 2, 3, 4})
                || Arrays.equals(var1, new int[]{2, 3, 4, 5})
                || Arrays.equals(var1, new int[]{3, 4, 5, 6})
                || Arrays.equals(var2, new int[]{1, 2, 3, 4})
                || Arrays.equals(var2, new int[]{2, 3, 4, 5})
                || Arrays.equals(var2, new int[]{3, 4, 5, 6});
    }

    static boolean isBigStreet(int[] dices) {
        Arrays.sort(dices);
        return Arrays.equals(dices, new int[]{1, 2, 3, 4, 5})
                || Arrays.equals(dices, new int[]{2, 3, 4, 5, 6});
    }

    static boolean isAllSame(int[] dices) {
        // NPE Check
        if (dices.length == 0) {
            return false;
        }

        int first = dices[0];
        for (int i = 1; i < dices.length; i++) {
            if (first != dices[i]) {
                return false;
            }
        }
        return true;
    }

    static boolean isPasch(int[] dices, int howMany) {
        Arrays.sort(dices);
        int number = 0, count = 0;
        for (int i : dices) {
            count = i > number ? 1 : ++count;
            if (count >= howMany)
                return true;
            number = i > number ? i : number;
        }
        return false;
    }

    static boolean isFullHouse(int[] dices) {
        Arrays.sort(dices);
        int number = 0, count = 0;
        for (int d : dices) {
            count = d > number ? 1 : ++count;
            if (count >= 3) {
                int finalNumber = number;
                return isAllSame(Arrays.stream(dices).filter(t -> t != finalNumber).toArray());
            }
            number = d > number ? d : number;
        }
        return false;
    }

}
