package de;

/**
 * Created by Shensu on 27.10.2016.
 */
public class Dice {

    public int value = 0;

    GameDices.Type type = GameDices.Type.fresh;

    public void change() {
        type = GameDices.Type.fresh.equals(type) ? GameDices.Type.keep : GameDices.Type.fresh;
    }

}
