package de;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;

import javax.swing.*;

/**
 * Created by vstange on 14.12.2016.
 */
public class Main {

    public static void main(String[] args) {
        try {
            // Look and Feel setzen
            UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        // Start it
        new KniffelUI().setVisible(true);
    }

}
