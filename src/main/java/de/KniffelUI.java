package de;

import com.jgoodies.forms.builder.FormBuilder;
import de.engine.GameEngine;
import de.engine.GamePhase;
import de.engine.GameState;
import de.engine.KniffelEventListener;
import de.forms.JBlockOneRow;
import de.forms.JDice;
import de.forms.JKniffelRow;
import de.forms.JResultRow;
import de.rules.KniffelEngine;
import de.rules.KniffelRule;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vstange
 */
public class KniffelUI extends JFrame implements KniffelEventListener {

    private List<JDice> keepLane = new ArrayList<>(5);

    private List<JDice> freshLane = new ArrayList<>(5);

    private JButton btnRoll = new JButton("Roll");

    private JButton btnNewGame = new JButton("New");

    private JLabel lblRoll = new JLabel();

    private List<KniffelEventListener> eventRows = new ArrayList<>();

    private GameEngine engine = new GameEngine();

    KniffelUI() {
        setTitle("Kniffel");
        setLocationRelativeTo(null);
        buildGui();
        prepareListeners();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void prepareListeners() {
        engine.addListener(this);
        for (KniffelEventListener row : eventRows) {
            engine.addListener(row);
        }
    }

    private void buildGui() {
        FormBuilder builder = FormBuilder.create();
        builder.columns("pref").rows("p,2dlu,p");
        builder.add(createPointPanel()).xy(1,1);
        builder.add(createRollPanel()).xy(1,3);
        setContentPane(builder.getPanel());
        pack();
    }

    private JPanel createRollPanel() {
        FormBuilder builder = FormBuilder.create();
        builder.columns("2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu");
        builder.rows("2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu");

        for (int i = 1; i <= 5; i++) {
            JDice dice = new JDice(this::shuffle);
            builder.add(dice).xy(i * 2, 2);
            keepLane.add(dice);
        }

        for (int i = 1; i <= 5; i++) {
            JDice dice = new JDice(this::shuffle);
            builder.add(dice).xy(i * 2, 4);
            freshLane.add(dice);
        }

        // Letzte Reihe bauen
        builder.add(btnNewGame).xy(2, 6);
        builder.addLabel("Wurf:").xy(6, 6, "r, d");
        builder.add(lblRoll).xy(8, 6, "l,d");
        builder.add(btnRoll).xy(10, 6);

        // Aktionen festlegen
        btnNewGame.addActionListener(e -> engine.prepareFreshGame());
        btnRoll.addActionListener(e -> engine.roll());

        return builder.build();
    }

    private JPanel createPointPanel() {
        FormBuilder builder = FormBuilder.create();
        builder.columns("pref").rows("2dlu");
        // Jede "Spiel-" bzw. Punkte-Zeile hinzufügen
        int i = 1;
        for (KniffelRule rule : KniffelEngine.blockOneRules) {
            i += 1;
            builder.appendRows("pref");
            JKniffelRow jKniffelRow = new JKniffelRow(rule, engine);
            eventRows.add(jKniffelRow);
            builder.add(jKniffelRow).xy(1, i);
        }

        {
            i += 1;
            builder.appendRows("pref");
            JBlockOneRow blockOnePointRow = new JBlockOneRow();
            eventRows.add(blockOnePointRow);
            builder.add(blockOnePointRow).xy(1, i);
        }

        for (KniffelRule rule : KniffelEngine.blockTwoRules) {
            i += 1;
            builder.appendRows("pref");
            JKniffelRow jKniffelRow = new JKniffelRow(rule, engine);
            eventRows.add(jKniffelRow);
            builder.add(jKniffelRow).xy(1, i);
        }

        {
            i += 1;
            builder.appendRows("pref");
            JResultRow resultRow = new JResultRow();
            eventRows.add(resultRow);
            builder.add(resultRow).xy(1, i);
        }

        return builder.build();
    }

    @Override
    public void refresh(GamePhase phase, GameState state) {
        lblRoll.setText(String.valueOf(state.rollCounter));

        // Kann noch würfeln? Nur bei Start- oder Roll-Phase
        boolean canRoll = phase == GamePhase.Start || phase == GamePhase.Roll;
        btnRoll.setEnabled(canRoll);

        // Kann die Würfel herumbewegen? Nur bei Roll- oder Decide-Phase
        boolean canShuffle = phase == GamePhase.Roll || phase == GamePhase.Decide;
        if (canShuffle)
            shuffle(null);
    }

    private void shuffle(Dice dice) {
        // Den zuletzte bewegten Würfel setzen
        engine.state.dices.setLast(dice);

        // Die Behalten-Reihe neu zeichnen
        List<Dice> keep = engine.state.dices.getKeep();
        int sizeKeep = keep.size();
        for (int i = 0; i < 5; i++) {
            keepLane.get(i).setDice(i < sizeKeep ? keep.get(i) : null);
        }

        // Die Würfel-Reihe neu zeichnen
        List<Dice> fresh = engine.state.dices.getFresh();
        int sizeFresh = fresh.size();
        for (int i = 0; i < 5; i++) {
            freshLane.get(i).setDice((i < sizeFresh ? fresh.get(i) : null));
        }
    }
}
