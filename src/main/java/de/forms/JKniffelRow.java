package de.forms;

import com.jgoodies.forms.builder.FormBuilder;
import de.engine.GameEngine;
import de.engine.GamePhase;
import de.engine.GameState;
import de.engine.KniffelEventListener;
import de.rules.KniffelRule;

import javax.swing.*;

/**
 * Created by vstange on 07.12.2016.
 */
public class JKniffelRow extends JPanel implements KniffelEventListener {

    private static ImageIcon missingIcon = new ImageIcon(ClassLoader.getSystemResource("missing.png"));

    private JButton btnPoints = new JButton("0");

    private JButton btnDiscard = new JButton(missingIcon);

    private JLabel lblPoints = new JLabel("");

    private int points = 0;

    private int possiblePoints = 0;

    private KniffelRule rule;

    private GameEngine engine;

    private boolean isFree = true;

    public JKniffelRow(KniffelRule rule, GameEngine engine) {
        this.rule = rule;
        this.engine = engine;
        FormBuilder builder = FormBuilder.create();
        builder.columns("2dlu, 140px, 22dlu, 16dlu, 18dlu, 16dlu, 18dlu").rows("pref");
        builder.panel(this);
        builder.debug(false);

        builder.addLabel(rule.getDisplay()).xy(2, 1);
        builder.add(btnPoints).xy(3, 1);
        builder.add(btnDiscard).xy(5, 1);
        builder.add(lblPoints).xy(7, 1);
        builder.build();

        // Punkte nehmen
        btnPoints.addActionListener(e -> {
            setPoints(possiblePoints);
        });

        // Discard Punkte
        btnDiscard.addActionListener(e -> {
            setPoints(0);
        });

        reset();
    }

    private void setPoints(int newPoints) {
        points = newPoints;
        lblPoints.setText(String.valueOf(points));
        isFree = false;
        engine.decision(points, rule.getType());
    }

    public int getPoints() {
        return points;
    }

    private void reset() {
        points = 0;
        possiblePoints = 0;
        isFree = true;
        lblPoints.setText("");
    }

    @Override
    public void refresh(GamePhase phase, GameState state) {
        if (phase == GamePhase.Start) {
            reset();
        }

        boolean isGameOn = phase == GamePhase.Roll || phase == GamePhase.Decide;

        int[] values = state.dices.getValues();

        // Punkte neu berechnen
        if (isFree) {
            possiblePoints = rule.getPoints(values);
            btnPoints.setText(String.valueOf(possiblePoints));
        }
        // Buttons freischalten bzw. sperren
        btnDiscard.setEnabled(isGameOn && isFree && points == 0);
        btnPoints.setEnabled(isGameOn && isFree && rule.isValid(values));
    }
}
