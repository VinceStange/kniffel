package de.forms;

import com.jgoodies.forms.builder.FormBuilder;
import de.engine.GamePhase;
import de.engine.GameState;
import de.engine.KniffelEventListener;
import de.rules.KniffelEngine;

import javax.swing.*;

/**
 * Created by vstange on 27.12.2016.
 */
public class JResultRow extends JPanel implements KniffelEventListener {

    private JLabel lblPoints = new JLabel("");

    public JResultRow() {
        FormBuilder builder = FormBuilder.create();
        builder.columns("2dlu, 140px, 22dlu, 16dlu, 18dlu, 16dlu, 18dlu").rows("pref");
        builder.panel(this);
        builder.debug(false);

        builder.addLabel("Gesamtpunkte").xy(2, 1);
        builder.add(lblPoints).xy(7, 1);
        builder.build();
    }

    @Override
    public void refresh(GamePhase phase, GameState state) {
        if (phase == GamePhase.Start) {
            lblPoints.setText("");
        }

        if (phase == GamePhase.Result) {
            lblPoints.setText(String.valueOf(state.pointsOverall));
        }
    }
}
