package de.forms;

import de.Dice;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.function.Consumer;

/**
 * Created by Shensu on 14.10.2016.
 */
public class JDice extends JPanel implements MouseListener {

    private static final int SIDE = 8;
    private Color color;
    private Dice dice = null;
    private Consumer<Dice> shuffle;

    public JDice(Consumer<Dice> shuffle) {
        this.shuffle = shuffle;
        color = Color.BLACK;
        setBorder(BorderFactory.createEtchedBorder(color, color.darker()));
        addMouseListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(SIDE * 7, SIDE * 7);
    }

    public void setDice(Dice dice) {
        this.dice = dice;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Würfel wählen, ob leer oder Würfel da ist
        color = dice == null ? Color.GRAY.brighter() : Color.black;
        // Rand zeichnen
        setBorder(BorderFactory.createEtchedBorder(color, color.darker()));
        if (dice == null)
            return;
        // Die Würfel-Punkte zeichnen mit der bisherigen Farbe
        g.setColor(color);
        switch (dice.value) {
            case 1:
                g.fillRect(3 * SIDE, 3 * SIDE, SIDE, SIDE);
                break;
            case 2:
                g.fillRect(5 * SIDE, SIDE, SIDE, SIDE);
                g.fillRect(SIDE, 5 * SIDE, SIDE, SIDE);
                break;
            case 3:
                g.fillRect(5 * SIDE, SIDE, SIDE, SIDE);
                g.fillRect(SIDE, 5 * SIDE, SIDE, SIDE);
                g.fillRect(3 * SIDE, 3 * SIDE, SIDE, SIDE);
                break;
            case 4:
                g.fillRect(SIDE, SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, 5 * SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, SIDE, SIDE, SIDE);
                g.fillRect(SIDE, 5 * SIDE, SIDE, SIDE);
                break;
            case 5:
                g.fillRect(SIDE, SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, 5 * SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, SIDE, SIDE, SIDE);
                g.fillRect(SIDE, 5 * SIDE, SIDE, SIDE);
                g.fillRect(3 * SIDE, 3 * SIDE, SIDE, SIDE);
                break;
            case 6:
                g.fillRect(SIDE, SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, 5 * SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, SIDE, SIDE, SIDE);
                g.fillRect(SIDE, 5 * SIDE, SIDE, SIDE);
                g.fillRect(SIDE, 3 * SIDE, SIDE, SIDE);
                g.fillRect(5 * SIDE, 3 * SIDE, SIDE, SIDE);
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (dice == null)
            return;
        dice.change();
        shuffle.accept(dice);
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
