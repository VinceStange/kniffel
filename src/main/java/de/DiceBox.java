package de;

import java.util.Random;

/**
 * A Box full of dices. Let's roll!
 */
public class DiceBox {

    Random random;

    public DiceBox() {
        random = new Random();
    }

    public int rollDice(int sides) {
        return random.nextInt(sides) + 1;
    }

    public int[] rollDices(int sides, int number) {
        int[] dices = new int[number];
        for (int i = 0; i < number; i++) {
            dices[i] = rollDice(sides);
        }
        return dices;
    }

}
