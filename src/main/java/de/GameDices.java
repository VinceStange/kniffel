package de;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Shensu on 15.10.2016.
 */
public class GameDices extends ArrayList<Dice> {

    enum Type {
        fresh, keep
    }

    public List<Dice> getKeep() {
        return this.stream().filter(d -> Type.keep.equals(d.type)).collect(Collectors.toList());
    }

    public List<Dice> getFresh() {
        return this.stream().filter(d -> Type.fresh.equals(d.type)).collect(Collectors.toList());
    }

    public void setValues(int[] values) {
        List<Dice> fresh = getFresh();
        for (int i = 0; i < fresh.size(); i++) {
            fresh.get(i).value = values[i];
        }
    }

    /**
     * Setzt den Würfel an die letzte Stelle.
     * @param dice der letzte aktive Würfel vom Rumschieben.
     */
    public void setLast(Dice dice) {
        if (dice != null) {
            remove(dice);
            add(dice);
        }
    }

    public int[] getValues() {
        return this.stream().mapToInt(d -> d.value).toArray();
    }

}
